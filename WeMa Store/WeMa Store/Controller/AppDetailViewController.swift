//
//  AppDetailViewController.swift
//  WeMa Store
//
//  Created by Niels Van den bout on 15-12-17.
//  Copyright © 2017 Niels Van den bout. All rights reserved.
//

import UIKit
import SDWebImage
import MessageUI

class AppDetailViewController: UIViewController, MFMailComposeViewControllerDelegate {

    @IBOutlet weak var downloadButton: UIButton!
    @IBOutlet weak var appIcon: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var bundleIdentifierLabel: UILabel!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var buildLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var developerLabel: UILabel!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    var app: App!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupAppInfo()
        setupViews()
        setupNav()
    }
    
    func setupNav() {
        navigationController?.navigationBar.shadowImage = UIImage()
        if self == navigationController?.viewControllers.first {
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Uitloggen", style: .plain, target: self, action: #selector(logOff))
        }
    }
    
    @objc func logOff() {
        let alert = UIAlertController(title: "Uitloggen", message: "Weet u zeker dat u wilt uitloggen?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Uitloggen", style: .default, handler: { (_) in
            self.navigationController?.dismiss(animated: true, completion: nil)
            UserDefaults.standard.removeObject(forKey: "user_name")
            UserDefaults.standard.removeObject(forKey: "owner_code")
        }))
        alert.addAction(UIAlertAction(title: "Annuleer", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func setupViews() {
        downloadButton.layer.cornerRadius = downloadButton.frame.size.height / 2
        moreButton.layer.cornerRadius = moreButton.layer.frame.size.height / 2
        appIcon.layer.cornerRadius = 25
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.indicator.isHidden = true
        if #available(iOS 11.0, *) {
        } else {
            navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        }
        
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    func getAppInfo() {
        guard let user_name = UserDefaults.standard.string(forKey: "user_name") else { return }
        guard let owner_code = UserDefaults.standard.string(forKey: "owner_code") else { return }
        indicator.startAnimating()
        indicator.isHidden = false
        API.sharedInstance.getApps(user_name, owner_code: owner_code) { (apps, error) in
            if let installed = apps?.installed, let newApps = apps?.newApps, let updates = apps?.updates {
                let apps = installed + newApps + updates
                if self.app != nil {
                    for app in apps {
                        if app.name == self.app.name {
                            self.app = app
                        }
                    }
                } else {
                    self.app = apps.first
                }
                self.setupAppInfo()
            }
            if error != nil || apps == nil {
                let alert = UIAlertController(title: "Geen verbinding", message: "Er is geen verbinding met onze server. Probeer het opnieuw.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
                   
                }))
                self.present(alert, animated: true, completion: nil)
            }
            self.indicator.stopAnimating()
        }
    }
    
    func setupAppInfo() {
        nameLabel.text = app.name
        buildLabel.text = app.latestVersion?.version_no
        versionLabel.text = app.latestVersion?.version_name
        descriptionLabel.text = app.latestVersion?.release_text
        dateLabel.text = app.latestVersion?.release_date
        bundleIdentifierLabel.text = app.package_name
        
        guard let url = URL(string: "https://www.iappministrator.com/market/icons/\(app.icon ?? "")") else { return }
        
        appIcon.sd_setImage(with: url) { (image, error, cache, url) in
            if let image = image {
                self.appIcon.image = image
                self.appIcon.layer.cornerRadius = 25
                self.appIcon.clipsToBounds = true
                self.appIcon.layer.borderColor = UIColor.init(hex: "ECECEC").cgColor
                self.appIcon.layer.borderWidth = 0.5
            }
        }
    }
    
    func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["niels.vandenbout@wemamobile.nl"])
            mail.setMessageBody("<p><br><br>App: <strong>\(app.name ?? "")</strong><br>Versie: <strong>\(app.latestVersion?.version_name ?? "")</strong><br>Build: <strong>\(app.latestVersion?.version_no ?? "")</strong><br></p>", isHTML: true)
            
            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    @IBAction func downloadApp(_ sender: UIButton) {
        guard let url = URL(string: "itms-services://?action=download-manifest&url=https://www.iappministrator.com/market/apks/\(app.latestVersion?.download_url ?? "")") else { return }
//        UIView.animate(withDuration: 0.3) {
//            sender.frame.size.width = 0.0
//            sender.setTitle("", for: .normal)
//            sender.backgroundColor = UIColor.white
//        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: { (_) in
                let alert = UIAlertController(title: "Installeren", message: "Bekijk de voortgang van de download op uw home scherm.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            })
        } else {
            UIApplication.shared.openURL(url)
            let alert = UIAlertController(title: "Installeren", message: "Bekijk de voortgang van de download op uw home scherm.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func showMoreOptions(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let refreshAction = UIAlertAction(title: "Ververs", style: .default, handler: { (_) in
            self.getAppInfo()
        })
        refreshAction.setValue(#imageLiteral(resourceName: "Refresh"), forKey: "image")
        alert.addAction(refreshAction)
        let feedbackAction = UIAlertAction(title: "Stuur feedback", style: .default, handler: { (_) in
            self.sendEmail()
        })
        feedbackAction.setValue(#imageLiteral(resourceName: "feedback_black_192x192"), forKey: "image")
        alert.addAction(feedbackAction)
        if let presentation = alert.popoverPresentationController {
            var frame = moreButton.frame
            frame.origin.x = -20
            frame.origin.y = 0
            presentation.sourceView = moreButton
            presentation.sourceRect = frame
            presentation.permittedArrowDirections = [.up, .right]
        }
        alert.addAction(UIAlertAction(title: "Annuleer", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
