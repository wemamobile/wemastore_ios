//
//  LoginViewController.swift
//  WeMa Store
//
//  Created by Niels Van den bout on 21-02-18.
//  Copyright © 2018 Niels Van den bout. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var userNameField: UITextField!
    @IBOutlet weak var codeField: UITextField!
    @IBOutlet weak var loginbutton: UIButton!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var userIcon: UIImageView!
    @IBOutlet weak var passwordIcon: UIImageView!
    @IBOutlet weak var headerImage: UIImageView!
    @IBOutlet weak var textToTop: NSLayoutConstraint!
    @IBOutlet weak var headerToTop: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    func setupHeader() {
        if  UIScreen.main.sizeType == UIScreen.SizeType.iPadNormal {
            headerImage.image = #imageLiteral(resourceName: "headerImageIpad")
            textToTop.constant = 100
        }
        
        if UIScreen.main.sizeType == UIScreen.SizeType.iPad12 {
            headerImage.image = #imageLiteral(resourceName: "headerImageIpad")
            textToTop.constant = 280
        }
        
        if UIScreen.main.sizeType == UIScreen.SizeType.iPad10 {
            headerImage.image = #imageLiteral(resourceName: "headerImageIpad")
            textToTop.constant = 150
        }
        
        if UIScreen.main.sizeType == UIScreen.SizeType.iPhone5 {
            headerImage.contentMode = .scaleAspectFit
            headerToTop.constant = -130
            textToTop.constant = 0
        }
        
        if UIScreen.main.sizeType == UIScreen.SizeType.iPhoneNormal {
            headerImage.contentMode = .scaleAspectFit
            headerToTop.constant = -100
            textToTop.constant = 40
        }
        
        if UIScreen.main.sizeType == UIScreen.SizeType.iPhonePlus {
            headerImage.contentMode = .scaleAspectFill
            headerToTop.constant = -100
            textToTop.constant = 70
        }
    }
    
    func setupViews() {
        loginbutton.layer.cornerRadius = 8
        userIcon.image = userIcon.image?.withRenderingMode(.alwaysTemplate)
        passwordIcon.image = passwordIcon.image?.withRenderingMode(.alwaysTemplate)
        userIcon.tintColor = UIColor(hex: "4D2773")
        passwordIcon.tintColor = UIColor(hex: "4D2773")
        setupHeader()
    }

    @IBAction func login(sender: UIButton) {
        indicator.startAnimating()
        indicator.isHidden = false
        
        UIView.animate(withDuration: 0.3, animations: {
            self.loginbutton.alpha = 0.0
        }) { (_) in
            self.loginbutton.isHidden = true
            API.sharedInstance.login(self.userNameField.text!, company_code: self.codeField.text!) { (response, error) in
                if let _ = response?.success, let user_id = response?.userId {
                    UserDefaults.standard.set(self.userNameField.text!, forKey: "user_name")
                    UserDefaults.standard.set(self.codeField.text!, forKey: "owner_code")
                    UserDefaults.standard.set(user_id, forKey: "user_id")
                    UserDefaults.standard.synchronize()
                    self.dismiss(animated: true, completion: nil)
                    self.updatePush()
                } else {
                    self.incorrectCredentials()
                }
                self.loginbutton.isHidden = false
                UIView.animate(withDuration: 0.3, animations: {
                    self.loginbutton.alpha = 1.0
                })
                self.indicator.stopAnimating()
            }
        }
        
        
    }
    
    func updatePush() {
        API.sharedInstance.updatePush { (app, error) in
            
        }
    }

    func incorrectCredentials() {
        let alert = UIAlertController(title: "Verificatie mislukt", message: "Uw gebruikersnaam of code is onjuist.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
