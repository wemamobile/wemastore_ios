//
//  LaunchViewController.swift
//  WeMa Store
//
//  Created by Niels Van den bout on 20-02-18.
//  Copyright © 2018 Niels Van den bout. All rights reserved.
//

import UIKit

class LaunchViewController: UIViewController {

    @IBOutlet weak var indicator: UIActivityIndicatorView!
    var apps = [App]()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getLatestVersion()
    }
    
    func getLatestVersion() {
        let storeBuild = Bundle.main.buildVersionNumber!
        let storeVersionInt = Int(storeBuild)!
        self.indicator.startAnimating()
        self.indicator.isHidden = false
        API.sharedInstance.getStoreVersion { (responseObject, error) in
            if responseObject != nil {
                if let responseVersion = responseObject?.latestVersion?.version_no, let responseVersionInt = Int(responseVersion), responseVersionInt > storeVersionInt {
                    if let url = responseObject?.latestVersion?.download_url {
                        self.getNewVersion(downloadUrl: url)
                    }
                } else {
                    self.getAppInfo()
                }
            } else {
                
            }
            self.indicator.stopAnimating()
        }
    }
    
    func noConnection() {
        let alert = UIAlertController(title: "Er is een probleem opgetreden", message: "Er is geen verbinding met onze server of uw account is ongeldig.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Opnieuw", style: .default, handler: { (_) in
            self.getAppInfo()
        }))
        alert.addAction(UIAlertAction(title: "Uitloggen", style: .default, handler: { (_) in
            self.showLogin()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func getNewVersion(downloadUrl: String) {
        let alert = UIAlertController(title: "Update", message: "Er is een update beschikbaar voor de WeMa Store. Installeer om verder te gaan.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Update", style: .default, handler: { (UIAlertAction) in
            self.indicator.startAnimating()
            self.indicator.isHidden = false
            if let url = URL(string: "itms-services://?action=download-manifest&url=https://www.iappministrator.com/market/apks/\(downloadUrl)") {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                }
            }
        }))
        present(alert, animated: true, completion: nil)
    }
    
    func delay(_ delay:Double, closure:@escaping ()->()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
    
    func getAppInfo() {
        delay(3) {
            if self.apps.count == 0 {
                self.indicator.startAnimating()
                self.indicator.isHidden = false
            }
        }
        guard let user_name = UserDefaults.standard.string(forKey: "user_name") else { showLogin(); return }
        guard let owner_code = UserDefaults.standard.string(forKey: "owner_code") else { showLogin(); return }
        API.sharedInstance.getApps(user_name, owner_code: owner_code) { (response, error) in
            
            if let installed = response?.installed, let newApps = response?.newApps, let updates = response?.updates {
                self.apps = installed + newApps + updates
                if self.apps.count == 1 {
                    self.performSegue(withIdentifier: "showSingleApp", sender: self.apps.first!)
                } else {
                    self.performSegue(withIdentifier: "showMultipleApps", sender: self.apps)
                }
            }

            if error != nil || response == nil {
                self.noConnection()
            } else {
                if self.apps.count == 0 {
                    let alert = UIAlertController(title: "Geen apps", message: "Er zijn geen apps gevonden voor deze inloggegevens. U wordt uitgelogd.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
                        self.showLogin()
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            self.indicator.stopAnimating()
        }
    }
    
    func showLogin() {
        performSegue(withIdentifier: "showLogin", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let nav = segue.destination as? UINavigationController, let vc = nav.topViewController as? AppDetailViewController {
            vc.app = sender as! App
        }
        if let nav = segue.destination as? UINavigationController, let vc = nav.topViewController as? AppsListViewController {
            vc.apps = sender as! [App]
        }
    }
}
