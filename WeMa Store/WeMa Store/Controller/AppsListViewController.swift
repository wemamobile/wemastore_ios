//
//  AppsListViewController.swift
//  WeMa Store
//
//  Created by Niels Van den bout on 20-02-18.
//  Copyright © 2018 Niels Van den bout. All rights reserved.
//

import UIKit

class AppsListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    var apps = [App]()
    let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        refreshControl.addTarget(self, action: #selector(getAppInfo), for: .valueChanged)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Uitloggen", style: .plain, target: self, action: #selector(logOff))
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
    }
    
    @objc func getAppInfo() {
        guard let user_name = UserDefaults.standard.string(forKey: "user_name") else { return }
        guard let owner_code = UserDefaults.standard.string(forKey: "owner_code") else { return }
        if apps.count == 0 {
            indicator.startAnimating()
            indicator.isHidden = false
        }
        API.sharedInstance.getApps(user_name, owner_code: owner_code) { (apps, error) in
            if let installed = apps?.installed, let newApps = apps?.newApps, let updates = apps?.updates {
                let apps = installed + newApps + updates
                if apps.count == 1 {
                    self.performSegue(withIdentifier: "showSingleApp", sender: apps.first!)
                } else {
                    self.apps = apps
                    self.tableView.reloadData()
                }
            }
            if error != nil || apps == nil {
                let alert = UIAlertController(title: "Geen verbinding", message: "Er is geen verbinding met onze server. Probeer het opnieuw.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
                    
                }))
                self.present(alert, animated: true, completion: nil)
            }
            self.indicator.isHidden = true
            self.refreshControl.endRefreshing()
        }
    }
    
    @objc func logOff() {
        let alert = UIAlertController(title: "Uitloggen", message: "Weet u zeker dat u wilt uitloggen?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Uitloggen", style: .default, handler: { (_) in
            self.navigationController?.dismiss(animated: true, completion: nil)
            UserDefaults.standard.removeObject(forKey: "user_name")
            UserDefaults.standard.removeObject(forKey: "owner_code")
        }))
        alert.addAction(UIAlertAction(title: "Annuleer", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.indicator.isHidden = true
        if #available(iOS 11.0, *) {
        } else {

            navigationController?.navigationBar.setBackgroundImage(#imageLiteral(resourceName: "white"), for: .default)
        }
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
    }
    
    func createCell(app: App) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "appCell") as! AppCell
        cell.appNameLabel.text = app.name
        cell.appUrl = app.latestVersion?.download_url ?? ""
        if let url = URL(string: "https://www.iappministrator.com/market/icons/\(app.icon ?? "")") {
            cell.appIcon.sd_setImage(with: url) { (image, error, cache, url) in
                if let image = image {
                    cell.appIcon.image = image
                    cell.appIcon.layer.cornerRadius = 14
                    cell.appIcon.layer.borderColor = UIColor.init(hex: "ECECEC").cgColor
                    cell.appIcon.layer.borderWidth = 0.5
                    cell.appIcon.clipsToBounds = true
                }
            }
        } else {
            cell.appIcon.layer.cornerRadius = 14
            cell.appIcon.layer.borderColor = UIColor.init(hex: "ECECEC").cgColor
            cell.appIcon.layer.borderWidth = 0.5
            cell.appIcon.clipsToBounds = true
        }
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? AppDetailViewController {
            vc.app = sender as! App
        }
    }
}

extension AppsListViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return createCell(app: apps[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return apps.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showSingleApp", sender: apps[indexPath.row])
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 76
    }
}

class AppCell: UITableViewCell {
    @IBOutlet weak var appIcon: UIImageView!
    @IBOutlet weak var appNameLabel: UILabel!
    @IBOutlet weak var developerLabel: UILabel!
    @IBOutlet weak var downloadButton: UIButton!
    
    var appUrl = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.downloadButton.layer.cornerRadius = self.downloadButton.frame.size.height / 2
    }
    
    @IBAction func downloadApp(sender: UIButton) {
        guard let url = URL(string: "itms-services://?action=download-manifest&url=https://www.iappministrator.com/market/apks/\(appUrl)") else { return }
        //        UIView.animate(withDuration: 0.3) {
        //            sender.frame.size.width = 0.0
        //            sender.setTitle("", for: .normal)
        //            sender.backgroundColor = UIColor.white
        //        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: { (_) in
                let alert = UIAlertController(title: "Installeren", message: "Bekijk de voortgang van de download op uw home scherm.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.parentViewController?.present(alert, animated: true, completion: nil)
            })
        } else {
            UIApplication.shared.openURL(url)
            let alert = UIAlertController(title: "Installeren", message: "Bekijk de voortgang van de download op uw home scherm.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.parentViewController?.present(alert, animated: true, completion: nil)
        }
    }
}
