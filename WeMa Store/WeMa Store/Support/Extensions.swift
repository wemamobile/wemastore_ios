//
//  Extensions.swift
//  CNIP
//
//  Created by Niels on 06-11-17.
//  Copyright © 2017 Niels. All rights reserved.
//

import UIKit

extension Date {
    var yesterday: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: self)!
    }
    var tomorrow: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: self)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    var isLastDayOfMonth: Bool {
        return tomorrow.month != month
    }
}

extension Bundle {
    
    var releaseVersionNumber: String? {
        return self.infoDictionary?["CFBundleShortVersionString"] as? String
    }
    
    var buildVersionNumber: String? {
        return self.infoDictionary?["CFBundleVersion"] as? String
    }
    
}

extension UIView {
    
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if parentResponder is UIViewController {
                return parentResponder as! UIViewController!
            }
        }
        return nil
    }
    
    var shadow: Bool {
        get {
            return layer.shadowOpacity > 0.0
        }
        set {
            if newValue == true {
                self.addShadow()
            }
        }
    }
    
    func addViewShadow() {
        let shadowLayer = CAShapeLayer()
        shadowLayer.path = UIBezierPath(roundedRect: self.bounds, cornerRadius: 6).cgPath
        shadowLayer.fillColor = UIColor.white.cgColor
        shadowLayer.shadowColor = UIColor.darkGray.cgColor
        shadowLayer.shadowPath = shadowLayer.path
        shadowLayer.shadowOffset = CGSize(width: 4.0, height: 4.0)
        shadowLayer.shadowOpacity = 0.3
        shadowLayer.shadowRadius = 8
        
        layer.insertSublayer(shadowLayer, at: 0)
    }
    
    func addShadow(_ shadowColor: CGColor = UIColor.black.cgColor,
                   shadowOffset: CGSize = CGSize(width: 3.0, height: 3.0),
                   shadowOpacity: Float = 0.3,
                   shadowRadius: CGFloat = 3.0) {
        layer.shadowColor = shadowColor
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = shadowRadius
    }
}

extension String{
    func convertHtml() -> NSAttributedString{
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        }catch {
            return NSAttributedString()
        }
    }
    
    func formatCorrectDate() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        guard let tempdate = dateFormatter.date(from: self) else {
            return ""
        }
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
        let stringDate = dateFormatter.string(from: tempdate)
        return stringDate
    }
}

//extension UIScreen {
//
//    enum SizeType: CGFloat {
//        case Unknown = 0.0
//        case iPhone4 = 960.0
//        case iPhone5 = 1136.0
//        case iPhoneNormal = 1334.0
//        case iPhonePlus = 1920.0
//        case iPhoneX = 2436.0
//        case iPadNormal = 2048.0
//        case iPad10 = 2224.0
//        case iPad12 = 2732.0
//    }
//
//    var sizeType: SizeType {
//        debugPrint(nativeBounds.height)
//        let height = nativeBounds.height
//        guard let sizeType = SizeType(rawValue: height) else { return .Unknown }
//        return sizeType
//    }
//}

extension UIScreen {
    
    enum SizeType: CGFloat {
        case Unknown = 0.0
        case iPhone4 = 480.0
        case iPhone5 = 568.0
        case iPhoneNormal = 667.0
        case iPhonePlus = 736.0
        case iPhoneX = 812.0
        case iPadNormal = 1024.0
        case iPad10 = 1112.0
        case iPad12 = 1366.0
    }
    
    var sizeType: SizeType {
        debugPrint(bounds.height)
        let height = bounds.height
        guard let sizeType = SizeType(rawValue: height) else { return .Unknown }
        return sizeType
    }
}

extension UIColor {
    convenience init(hex: String) {
        let string = hex.replacingOccurrences(of: "#", with: "")
        let scanner = Scanner(string: string)
        scanner.scanLocation = 0
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }
}

extension DateFormatter {
    
    convenience init(withFormat format: String) {
        self.init()
        self.dateFormat = format
    }
}

