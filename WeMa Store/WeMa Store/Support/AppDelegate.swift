//
//  AppDelegate.swift
//  WeMa Store
//
//  Created by Niels Van den bout on 15-12-17.
//  Copyright © 2017 Niels Van den bout. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Pushwoosh

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, PushNotificationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        window?.backgroundColor = UIColor.white
        IQKeyboardManager.sharedManager().enable = true
        
        PushNotificationManager.push().delegate = self
        // set default Pushwoosh delegate for iOS10 foreground push handling
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = PushNotificationManager.push().notificationCenterDelegate
        }
        UIApplication.shared.applicationIconBadgeNumber = 0
        // track application open statistics
        PushNotificationManager.push().sendAppOpen()
        
        // register for push notifications!
        PushNotificationManager.push().registerForPushNotifications()
        
        return true
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        PushNotificationManager.push().handlePushRegistration(deviceToken as Data!)
        // get push token
        let notificationType = UIApplication.shared.currentUserNotificationSettings!.types
        if notificationType == [] {
            debugPrint("🍒A. Failure")
        } else {
            debugPrint("🍒A. Success")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ApplicationDidRegisterUserNotificationSettingsNotification"), object: self)
        }
    }
    
    func onDidFailToRegisterForRemoteNotificationsWithError(_ error: Error!) {
        debugPrint("onDidFailToRegisterForRemoteNotificationsWithError")
    }
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        debugPrint("didRegister")
    }
    
    func onDidRegisterForRemoteNotifications(withDeviceToken token: String!) {
        debugPrint("🍒PUSH-ID FROM APP-DELGATE. token: \(token)")
        UserDefaults.standard.setValue(token, forKey: "push_id")
        UserDefaults.standard.synchronize()
        // always update the push settings
        subscribePushSettings()
    }
    
    func subscribePushSettings() {
        API.sharedInstance.updatePush { (response, error) in
            if error ==  nil {
                debugPrint("🍒Push subscribed with settings")
            }
        }
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        if let vc = UIApplication.shared.visibleViewController as? AppsListViewController {
            vc.getAppInfo()
        } else if let vc = UIApplication.shared.visibleViewController as? AppDetailViewController {
//            if vc == vc.navigationController?.viewControllers.first {
                vc.getAppInfo()
//            }
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

extension UIApplication {
    
    var visibleViewController: UIViewController? {
        
        guard let rootViewController = keyWindow?.rootViewController else {
            return nil
        }
        
        return getVisibleViewController(rootViewController)
    }
    
    private func getVisibleViewController(_ rootViewController: UIViewController) -> UIViewController? {
        
        if let presentedViewController = rootViewController.presentedViewController {
            return getVisibleViewController(presentedViewController)
        }
        
        if let navigationController = rootViewController as? UINavigationController {
            return navigationController.visibleViewController
        }
        
        if let tabBarController = rootViewController as? UITabBarController {
            return tabBarController.selectedViewController
        }
        
        return rootViewController
    }
}

