//
//  LoginResponse.swift
//  WeMa Store
//
//  Created by Niels Van den bout on 15-12-17.
//  Copyright © 2017 Niels Van den bout. All rights reserved.
//

import Foundation

struct LoginResponse: Codable {
    var success: Bool?
    var userId: String?
}
