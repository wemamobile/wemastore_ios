//
//  API.swift
//  DGL
//
//  Created by Niels on 25-09-17.
//  Copyright © 2017 Niels. All rights reserved.
//

import Foundation
import Alamofire

class API {
    
    class var sharedInstance: API {
        struct Singleton {
            static let instance = API()
        }
        return Singleton.instance
    }
    
    func getApps(_ user_name: String, owner_code: String, completionHandler: @escaping (_ responseObject: AppResponse?, _ error: Error?) -> ()) {
        Alamofire.request(Router.getApps(["user_name" : user_name, "owner_code": owner_code, "platform": "iOS"]))
            .responseObject(completionHandler: { (response: DataResponse<AppResponse>) in
                completionHandler(response.result.value, response.result.error)
            })
    }
    
    func login(_ user_name: String, company_code: String, completionHandler: @escaping (_ responseObject: LoginResponse?, _ error: Error?) -> ()) {
        Alamofire.request(Router.login(["user_name" : user_name, "company_code": company_code]))
            .responseObject(completionHandler: { (response: DataResponse<LoginResponse>) in
                completionHandler(response.result.value, response.result.error)
            })
    }
    
    func getStoreVersion(completionHandler: @escaping (_ responseObject: App?, _ error: Error?) -> ()) {
        Alamofire.request(Router.checkStoreVersion(["platform": "iOS"]))
            .responseObject(completionHandler: { (response: DataResponse<App>) in
                completionHandler(response.result.value, response.result.error)
            })
    }
    
    func updatePush(completionHandler: @escaping (_ responseObject: App?, _ error: Error?) -> ()) {
        guard let userName = UserDefaults.standard.string(forKey: "user_id") else { return }
        guard let push_id = UserDefaults.standard.string(forKey: "push_id") else { return }
        Alamofire.request(Router.updatePush(["user_id": userName, "push_id": push_id]))
            .responseObject(completionHandler: { (response: DataResponse<App>) in
                completionHandler(response.result.value, response.result.error)
            })
    }
    
    
    //    user_name
    //    push_id


}

