//
//  DataRequest.swift
//  MijnHarwig
//
//  Created by Niels on 29-09-17.
//  Copyright © 2017 Niels. All rights reserved.
//

import Foundation
import Alamofire

// MARK: - Object

extension Request {
    
    /// Returns a result data type that contains the response data as an object of the specified type.
    ///
    /// - parameter response: The response from the server.
    /// - parameter data:     The data returned from the server.
    /// - parameter error:    The error already encountered if it exists.
    ///
    /// - returns: The result data type.
    public static func serializeResponseObject<T: Decodable>(response: HTTPURLResponse?, data: Data?, error: Error?) -> Result<T> {
        guard error == nil else { return .failure(error!) }
        
        guard let validData = data else {
            return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
        }
        
        // Deserialize the data into an object
        do {
            let object = try JSONDecoder().decode(T.self, from: validData)
            return .success(object)
        }
        catch {
            return .failure(AFError.responseSerializationFailed(reason: .jsonSerializationFailed(error: error)))
        }
    }
}

public extension DataRequest {
    
    /// Creates a response serializer that returns the associated data as an object of the specified type.
    ///
    /// - returns: An object response serializer.
    public static func objectResponseSerializer<T: Decodable>() -> DataResponseSerializer<T> {
        return DataResponseSerializer { (_, response, data, error) in
            return Request.serializeResponseObject(response: response, data: data, error: error)
        }
    }
    
    /// Adds a handler to be called once the request has finished.
    ///
    /// - parameter completionHandler: The code to be executed once the request has finished.
    ///
    /// - returns: The request.
    @discardableResult
    public func responseObject<T: Decodable>(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(
            queue: queue,
            responseSerializer: DataRequest.objectResponseSerializer(),
            completionHandler: completionHandler
        )
    }
}
