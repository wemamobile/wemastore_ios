
//  Router.swift
//  MijnHarwig
//
//  Created by Martijn Smit on 09/05/2017.
//  Copyright © 2017 WeMa Mobile B.V. All rights reserved.
//

import Foundation
import Alamofire

enum Router: URLRequestConvertible {
    static let baseURL = "https://www.iappministrator.com/market/api"
    
    // calls
    case login(Parameters)
    case getApps(Parameters)
    case checkStoreVersion(Parameters)
    case adjustInstallInfo
    case updatePush(Parameters)
    
    var method: HTTPMethod {
        return .post
    }
    
    var path: String {
        switch self {
        case .login:
            return "check_login.php"
        case .getApps:
            return "fetch_app_list.php"
        case .checkStoreVersion:
            return "self_update_check.php"
        case .adjustInstallInfo:
            return "adjust_app_install_info.php"
        case .updatePush:
            return "_update_push_id.php"
        }
    }
    
    // MARK: URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        
        let url: URL = try Router.baseURL.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        switch self {
        case .login(let parameters):
           urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .getApps(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .checkStoreVersion(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .updatePush(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        default:
            urlRequest = try URLEncoding.default.encode(urlRequest, with: nil)
        }
        return urlRequest
    }
}


