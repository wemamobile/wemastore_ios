//
//  DefaultResponse.swift
//  WeMa Store
//
//  Created by Niels Van den bout on 15-12-17.
//  Copyright © 2017 Niels Van den bout. All rights reserved.
//

import Foundation

struct AppResponse: Codable {
    let updates: [App]?
    let installed: [App]?
    let newApps: [App]?
}

struct App: Codable {
    let market_id: String?
    let name: String?
    let package_name: String?
    let platform: String?
    let icon: String?
    let latestVersion: LatestVersion?
}

struct LatestVersion: Codable {
    let market_id: String?
    let version_no: String?
    let version_name: String?
    let version_type: String?
    let release_date: String?
    let release_text: String?
    let download_url: String?
}

